<?php

namespace Bundle\UserBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Profile
 *
 * @ORM\Table(name="user_profile")
 * @ORM\Entity
 */
class Profile {

    /**
     * @var integer
     * @ORM\id
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="id" ,referencedColumnName="id")
     * */
    private $id;

    /**
     * @ORM\Column(name="address", nullable=true , type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(name="name", nullable=true , type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(name="phone", nullable=true , type="string", length=255)
     */
    private $phone;


    /**
     * Set address
     *
     * @param string $address
     * @return Profile
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }
    /**
     * Set address
     *
     * @param string $name
     * @return Profile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set phone
     *
     * @param string $phone
     * @return Profile
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set id
     *
     * @param \Bundle\UserBundle\Entity\User $id
     * @return Profile
     */
    public function setId(\Bundle\UserBundle\Entity\User $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return \Bundle\UserBundle\Entity\User 
     */
    public function getId()
    {
        return $this->id;
    }
}
