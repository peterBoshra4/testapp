<?php

namespace Bundle\CMSBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Blog
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="blog")
 * @ORM\Entity(repositoryClass="\Bundle\CMSBundle\Repository\BlogRepository")
 */
class Blog {
   // Constants  For  Status
    const PENDING = 100;
    const PUBLISHED = 101;
    const DRAFT = 102;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(name="tittle", type="string", length=45)
     */
    private $tittle;
    

    /**
     * @var object
     * @Assert\NotBlank()
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="BlogCategory", inversedBy="blogs")
     */
    protected $category;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\ManyToOne(targetEntity="\Bundle\UserBundle\Entity\User", inversedBy="blogs")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="blog")
     */
    protected $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Destination
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     */
    public function updatedTimestamps() {
        $this->setCreated(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    public function __construct() {
        $this->comments = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tittle
     *
     * @param string $tittle
     * @return Blog
     */
    public function setTittle($tittle) {
        $this->tittle = $tittle;

        return $this;
    }

    /**
     * Get tittle
     *
     * @return string 
     */
    public function getTittle() {
        return $this->tittle;
    }


    /**
     * Set category
     *
     * @param \Bundle\CMSBundle\Entity\BlogCategory $category
     * @return Blog
     */
    public function setCategory(\Bundle\CMSBundle\Entity\BlogCategory $category = null) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Bundle\CMSBundle\Entity\BlogCategory
     */
    public function getCategory() {
        return $this->category;
    }

    /**
     * Set user
     *
     * @param \Bundle\UserBundle\Entity\User $user
     * @return Blog
     */
    public function setUser(\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Bundle\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Blog
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Blog
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add comments
     *
     * @param \Bundle\CMSBundle\Entity\Comment $comments
     * @return Blog
     */
    public function addComment(\Bundle\CMSBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Bundle\CMSBundle\Entity\Comment $comments
     */
    public function removeComment(\Bundle\CMSBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }
}
