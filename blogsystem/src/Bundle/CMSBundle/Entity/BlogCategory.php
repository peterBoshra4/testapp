<?php

namespace Bundle\CMSBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlogCategory
 *
 * @ORM\Table(name="blog_category")
 * @ORM\Entity
 */
class BlogCategory {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tittle", type="string", length=45)
     */
    private $tittle;

    /**
     * @var string
     *
     * @ORM\Column(name="description" ,nullable=true , type="text")
     */
    private $description;
    
    /**
     * @ORM\OneToMany(targetEntity="Blog", mappedBy="category")
     */
    protected $blogs;

    public function __construct() {
        $this->blogs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set tittle
     *
     * @param string $tittle
     * @return BlogCategory
     */
    public function setTittle($tittle) {
        $this->tittle = $tittle;

        return $this;
    }

    /**
     * Get tittle
     *
     * @return string 
     */
    public function getTittle() {
        return $this->tittle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return BlogCategory
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    public function __toString() {
        return $this->getTittle();
    }


    /**
     * Add blogs
     *
     * @param \Bundle\CMSBundle\Entity\Blog $blogs
     * @return BlogCategory
     */
    public function addBlog(\Bundle\CMSBundle\Entity\Blog $blogs)
    {
        $this->blogs[] = $blogs;
    
        return $this;
    }

    /**
     * Remove blogs
     *
     * @param \Bundle\CMSBundle\Entity\Blog $blogs
     */
    public function removeBlog(\Bundle\CMSBundle\Entity\Blog $blogs)
    {
        $this->blogs->removeElement($blogs);
    }

    /**
     * Get blogs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBlogs()
    {
        return $this->blogs;
    }
}
