<?php

namespace Bundle\CMSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tittle','text',array('label' => 'Title'))
            ->add('description','textarea',array('label' => 'Description' ,  'attr' => array(
                'class' => 'tinymce',
                'data-theme' => 'bbcode' // Skip it if you want to use default theme
            )))
            ->add('category')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => '\Bundle\CMSBundle\Entity\Blog'
        ));
    }

    public function getName()
    {
        return 'md_bundle_cmsbundle_blogtype';
    }
}
