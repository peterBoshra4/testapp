<?php

namespace Bundle\CMSBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlogCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
             ->add('tittle', null, array('label' => 'Blog Category Title'))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bundle\CMSBundle\Entity\BlogCategory'
        ));
    }

    public function getName()
    {
        return 'bundle_cmsbundle_blogcategorytype';
    }
}
