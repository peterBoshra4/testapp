<?php

namespace Bundle\CMSBundle\Controller\Administration;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Bundle\CMSBundle\Entity\Blog;
use Bundle\CMSBundle\Form\BlogType;

/**
 * Blog controller.
 *
 * @Route("/blog")
 */
class BlogController extends Controller {

    /**
     * Lists all Blog entities.
     *
     * @Route("/", name="blog")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CMSBundle:Blog')->findAll();
        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Blog entity.
     *
     * @Route("/", name="blog_create")
     * @Method("POST")
     * @Template("CMSBundle:Administration/Blog:edit.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Blog();
        $form = $this->createForm(new BlogType(), $entity);
        $form->bind($request);
        $status = $this->container->get('request_stack')->getCurrentRequest()->get('status');

        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
              $currentAdmin = $this->getUser();
              $entity->setStatus($status);
              $entity->setUser($currentAdmin);
             $em->persist($entity);
             $em->flush();
             return $this->redirect($this->generateUrl('blog_edit', array('id' => $entity->getId())));
            }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Blog entity.
     *
     * @Route("/new", name="blog_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Blog();
        $form = $this->createForm(new BlogType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'published' => Blog::PUBLISHED ,
            'pending' => Blog::PENDING ,
            'draft' => Blog::DRAFT ,
        );
    }

    /**
     * Displays a form to edit an existing Blog entity.
     *
     * @Route("/{id}/edit", name="blog_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Blog')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog entity.');
        }
        $editForm = $this->createForm(new BlogType(), $entity);
        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'published' => Blog::PUBLISHED ,
            'pending' => Blog::PENDING ,
            'draft' => Blog::DRAFT ,
        );
    }

    /**
     * Edits an existing Blog entity.
     *
     * @Route("/{id}", name="blog_update")
     * @Method("PUT")
     * @Template("CMSBundle:Blog:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CMSBundle:Blog')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog entity.');
        }
        $status = $this->container->get('request_stack')->getCurrentRequest()->get('status');
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new BlogType(), $entity);
        $editForm->bind($request);
        if ($editForm->isValid()) {
            $entity->setStatus($status);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('blog'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Blog entity.
     *
     * @Route("/delete", name="blog_delete")
     * @Method("POST")
     */
    public function deleteAction() {
        $id = $this->container->get('request_stack')->getCurrentRequest()->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:Blog')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Blog entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('blog'));
    }

    /**
     * Creates a form to delete a Blog entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }
}
