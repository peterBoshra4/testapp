<?php

namespace Bundle\CMSBundle\Controller\Administration;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Bundle\CMSBundle\Entity\BlogCategory;
use Bundle\CMSBundle\Form\BlogCategoryType;

/**
 * BlogCategory controller.
 *
 * @Route("/bcat")
 */
class BlogCategoryController extends Controller {

    /**
     * Lists all BlogCategory entities.
     *
     * @Route("/", name="bcat")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CMSBundle:BlogCategory')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new BlogCategory entity.
     *
     * @Route("/", name="blogcategory_create")
     * @Method("POST")
     * @Template("CMSBundle:BlogCategory:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new BlogCategory();
        $form = $this->createForm(new BlogCategoryType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('bcat'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new BlogCategory entity.
     *
     * @Route("/new", name="blogcategory_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new BlogCategory();
        $form = $this->createForm(new BlogCategoryType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing BlogCategory entity.
     *
     * @Route("/{id}/edit", name="blogcategory_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CMSBundle:BlogCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlogCategory entity.');
        }

        $editForm = $this->createForm(new BlogCategoryType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }      


    /**
     * Edits an existing BlogCategory entity.
     *
     * @Route("/{id}", name="blogcategory_update")
     * @Method("PUT")
     * @Template("CMSBundle:BlogCategory:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CMSBundle:BlogCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlogCategory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new BlogCategoryType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('blogcategory_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a BlogCategory entity.
     *
     * @Route("/delete", name="blogcategory_delete")
     * @Method("POST")
     */
    public function deleteAction() {
        $id = $this->container->get('request_stack')->getCurrentRequest()->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('CMSBundle:BlogCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlogCategory entity.');
        }
        $em->remove($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('bcat'));
    }

    /**
     * Creates a form to delete a BlogCategory entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
