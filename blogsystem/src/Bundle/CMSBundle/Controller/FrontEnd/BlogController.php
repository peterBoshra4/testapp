<?php

namespace Bundle\CMSBundle\Controller\FrontEnd;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Bundle\CMSBundle\Entity\Blog;
use Bundle\CMSBundle\Entity\Comment as Comment;
use Bundle\CMSBundle\Form\BlogType;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

use Bundle\CMSBundle\Repository\BlogRepository;

// I didn't use annotations here i just make it direct from routing
class BlogController extends Controller
{


    /*
     * show all Blogs with categories filter
     */
    public function blogListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $category = $this->container->get('request_stack')->getCurrentRequest()->get('c');
        $search = new \stdClass();
        $search->category = $category;
        $blogs = $em->getRepository('CMSBundle:Blog')->filter($search);
        $categories = $em->getRepository('CMSBundle:BlogCategory')->findAll();
        return $this->render('CMSBundle:FrontEnd/Blog:blogs.html.twig', array(
            'blogs' => $blogs,
            'search' => $search,
            'categories' => $categories,
        ));

    }

    public function blogDetailsAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $blog = $em->getRepository('CMSBundle:Blog')->find($id);
        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog .');
        }
        $comments = $blog->getComments();
        return $this->render('CMSBundle:FrontEnd/Blog:blogDetails.html.twig', array(
            'blog' => $blog,
            'comments' => $comments,
        ));
    }
/*
 * add new comment form submit
 */
    public function addCommentAction()
    {
        $em = $this->getDoctrine()->getManager();

        $request = $this->container->get('request_stack')->getCurrentRequest();
        $blog_id = $request->request->get('blog_id');

        $blog = $em->getRepository('CMSBundle:Blog')->find($blog_id);
        if (!$blog) {
            throw $this->createNotFoundException('Unable to find Blog .');
        }
        $comment = $this->insertComment($request, $blog);
        return $this->redirect($this->generateUrl('blog_details', array('id' => $blog_id)));


    }

    /*
   * insert comment in the DB
   */
    function insertComment($request, $blog)
    {
        // get current user ip
        $ip = $this->generateAnonId($request);
        $desc = $request->request->get('desc');
        $em = $this->getDoctrine()->getManager();
        $comment = new Comment();
        $comment->setBlog($blog);
        $comment->setDescription($desc);
        $comment->setUserIp($ip);
        $em->persist($comment);
        $em->flush();


    }
    // generated key dependent in the cookie to sanctify users if we need to make each user add only one comment
    public function generateAnonId($request){
        $cookies = $request->cookies;
        if ($cookies->has('anon_id'))
        {
            $anon_id = $cookies->get('anon_id');
        }else {
            $token = md5(uniqid(rand(), true));
            $cookie = new Cookie('anon_id', $token, (time() + 3600 * 24 * 7), '/');
            $response = new Response();
            $response->headers->setCookie($cookie);
            $response->send();
            $anon_id=$token;
        }
        return $anon_id;
    }

}
