<?php

namespace Bundle\CMSBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Bundle\CMSBundle\Entity\Blog As blog;

class BlogRepository extends EntityRepository {


/*
 * Filter function with custom query which we can use to add any search criteria we want
 */
    public function filter($search) {
        $check = false;
        $sqlCondition = " ";
        $published = blog::PUBLISHED;
        $sqlCondition = " WHERE b.status = $published ";
        if ($search->category != NULL) {
                $sqlCondition .= " AND b.category_id =  $search->category ";

            }
            $sql = ' SELECT id  FROM blog b ' .
                    $sqlCondition;
            $filterResult = $this->getEntityManager()->getConnection()->executeQuery($sql);
            $result = array();
            foreach ($filterResult as $key => $r) {
                $result[$key] = $this->getEntityManager()->getRepository('CMSBundle:Blog')->find($r['id']);
            }

        return $result;

    }


}